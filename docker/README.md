To build docker images, the command shall be launched in the project root folder. They may also be created through CI/CD using Gitlab Web interface (see [here](../CI.md)).

```shell
docker build -f docker/<dockerfile> .
```

The .dockerignore file is used to reduce docker image size by keeping the docker context to minimum.

There are several Dockerfiles here:

- [Dockerfile.xeus-cling], which creates an image with a Conda environment to run properly Xeus-cling.
- [Dockerfile.fedora_for_hands_on] which provides an environment with both clang and gcc installed. This notebook is not intended to run the notebooks but may be used to run the hands-ons.
- [Dockerfile.fedora_with_boost] is used along with [the notebook about third party warnings](../6-InRealEnvironment/4-ThirdParty.ipynb).

