{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# [Getting started in C++](./) - [Object programming](./0-main.ipynb) - [(Base) constructors and destructor](./3-constructors-destructor.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction to base constructor\n",
    "\n",
    "In fact, our previous `Init()` function is meant to be realized through a dedicated method called a **constructor**. By convention, a constructor shares the name of the struct or class.\n",
    "\n",
    "Several constructors may be defined for a given class, provided there is no signature overlap."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "struct Vector\n",
    "{\n",
    "    double x_;\n",
    "    double y_;    \n",
    "    double z_;\n",
    "    \n",
    "    Vector(); // Constructor\n",
    "    \n",
    "    Vector(double x, double y, double z); // Another constructor\n",
    "    \n",
    "    double Norm() const;\n",
    "};"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Data attributes may be initialized more efficiently with a special syntax shown below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Vector::Vector(double x, double y, double z)\n",
    ": x_(x), // See the syntax here: `:` to introduce data attributes initialization,\n",
    "y_(y), // and commas to separate the different data attributes.\n",
    "z_(z)\n",
    "{ \n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cppmagics cppyy/cppdef\n",
    "// < without this line the perfectly valid - and recommended! - braces in data \n",
    "// attribute initialization are not accepted by our kernel.\n",
    "    \n",
    "Vector::Vector()\n",
    ": x_{0}, // braces may also be used since C++ 11, and are the recommended choice \n",
    "y_{}, // the value inside may be skipped if it is 0\n",
    "z_{}\n",
    "{ }"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "double Vector::Norm() const\n",
    "{\n",
    "    return std::sqrt(x_ * x_ + y_ * y_ + z_ * z_);\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "{\n",
    "    Vector v(5., 6., -4.2); // note the creation of an object with a constructor call.\n",
    "    std::cout << v.Norm() << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### **[WARNING]** How to call a constructor without argument\n",
    "\n",
    "There is a technicality for constructor without arguments: they must be called **without** parenthesis (the reason is a possible confusion with a [functor](../3-Operators/5-Functors.ipynb) - see Item 6 of [More Effective C++](../bibliography.ipynb#Effective-C++-/-More-Effective-C++) or [this blog post](https://www.fluentcpp.com/2018/01/30/most-vexing-parse/) if you want to learn more about the reasons of this):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "{\n",
    "    Vector v; // no parenthesis here!\n",
    "    std::cout << v.Norm() << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### \"Auto-to-stick\" syntax for constructor calls\n",
    "\n",
    "A way to avoid the mistake entirely is to call the so-called \"auto-to-stick\" alternate syntax:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "{\n",
    "    auto v = Vector(5, 10, 15); // auto-to-stick syntax: a new perfectly fine way to declare an object.\n",
    "    std::cout << v.Norm() << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This syntax completely removes the ambiguity: you **have to** keep the `()`, so the constructor with no arguments doesn't become a special case that is handled differently."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    auto v = Vector(); // auto-to-stick syntax\n",
    "    std::cout << v.Norm() << std::endl;    \n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This auto-to-stick syntax is not widely used, but is advised by some developers as a natural evolution of the syntax of the language (it is very akin to the [alternate syntax for functions](../1-ProceduralProgramming/4-Functions.ipynb#Alternate-function-syntax) we saw earlier). \n",
    "\n",
    "I advise you to read this very interesting [FluentCpp post](https://www.fluentcpp.com/2018/09/28/auto-stick-changing-style/) about this syntax which ponders about the relunctance we might have to embrace evolution of our languages - so the reading is of interest even for developers using other languages."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The importance of the `:` syntax\n",
    "\n",
    "We saw just above that we may init data attributes either in the body of the constructor or before the body with the `:` syntax.\n",
    "\n",
    "In the above example, both are fine and work as expected.\n",
    "\n",
    "However, there are several cases for which you **must** use the `:` syntax; here are two of them.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "struct First\n",
    "{ };"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "struct Second\n",
    "{\n",
    "    \n",
    "    const First& first_;\n",
    "    \n",
    "    Second(const First& first, int value);\n",
    "\n",
    "    const int value_;\n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Second::Second(const First& first, int value)\n",
    "{\n",
    "    first_ = first; // COMPILATION ERROR: can't initialize a reference data attribute here!    \n",
    "\n",
    "    value_ = value; // COMPILATION ERROR: can't change the value of a const variable!\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Second::Second(const First& first, int value)\n",
    ": first_(first), // OK!\n",
    "value_(value)\n",
    "{ } "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With the `:` syntax, the attributes are filled with their expected values _as soon as_ the object is created.\n",
    "\n",
    "On the other hand, the body of the constructor is run _after_ the actual creation occurs.\n",
    "\n",
    "So concerning data attributes if their value is set in the body of the constructor it is actually an _assignment_ that takes place and replace the default value built at construction.\n",
    "\n",
    "The cases in which the data attributes **must** be defined by the `:` constructor are:\n",
    "\n",
    "- When the data attribute can't be copied (this case covers both cases already seen).\n",
    "- When the type of the data attribute doesn't foresee a default constructor (i.e. a constructor without arguments).\n",
    "\n",
    "Anyway, when you can you should really strive to use the `:` syntax to define data attributes.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Good practice: In the `:` syntax to initialize data attributes, define them in the same order as in the class declaration"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For instance, don't do that:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "struct PoorlyDefinedVector\n",
    "{\n",
    "    double x_;\n",
    "    double y_;\n",
    "\n",
    "    PoorlyDefinedVector(double x, double y);\n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "PoorlyDefinedVector::PoorlyDefinedVector(double x, double y)\n",
    ": y_(y), // y_ is defined first, whereas x_ comes first in data attribute list!\n",
    "x_(x)\n",
    "{ }\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You may have unwanted effects if you do not respect the same ordering.\n",
    "\n",
    "Fortunately, compilers are able to emit warnings to advise you to remedy this if you have properly activated them with options such as `-Wall` (we shall discuss this in a [later notebook](../6-InRealEnvironment/3-Compilers.ipynb)). Here is an example when calling directly `clang`; you may also try with `gcc` which will also provides a warning:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cppmagics clang\n",
    "\n",
    "#include <cstdlib>\n",
    "\n",
    "struct PoorlyDefinedVector\n",
    "{\n",
    "    double x_;\n",
    "    double y_;\n",
    "\n",
    "    PoorlyDefinedVector(double x, double y);\n",
    "};\n",
    "\n",
    "PoorlyDefinedVector::PoorlyDefinedVector(double x, double y)\n",
    ": y_(y), // y_ is defined first, whereas x_ comes first in data attribute list!\n",
    "x_(x)\n",
    "{ }\n",
    "\n",
    "int main()\n",
    "{\n",
    "    return EXIT_SUCCESS;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Delegating constructor\n",
    "\n",
    "Since C++ 11, it is possible to use a base constructor when defining another constructor:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "struct Vector2\n",
    "{\n",
    "    double x_, y_, z_;\n",
    "    \n",
    "    Vector2();\n",
    "    \n",
    "    Vector2(double x);\n",
    "    \n",
    "    Vector2(double x, double y, double z);\n",
    "    \n",
    "    void Print() const;\n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "void Vector2::Print() const\n",
    "{\n",
    "    std::cout << \"(x, y, z) = (\" << x_ << \", \" << y_ << \", \" << z_ << \")\" << std::endl << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "Vector2::Vector2()\n",
    ": x_(-1.),\n",
    "y_(-1.),\n",
    "z_(-1.)\n",
    "{\n",
    "    std::cout << \"Calling Vector2 constructor with no arguments.\" << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Vector2::Vector2(double x, double y, double z)\n",
    ": x_(x),\n",
    "y_(y),\n",
    "z_(z)\n",
    "{ }"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Vector2::Vector2(double x)\n",
    ": Vector2() \n",
    "{ \n",
    "    x_ = x; // As the first constructor is assumed to build fully the object, you can't assign data attributes \n",
    "            // before the body of the constructor.\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    std::cout << \"Constructor with no argument:\" << std::endl;\n",
    "    Vector2 v1;\n",
    "    v1.Print();\n",
    "    \n",
    "    std::cout << \"Constructor with no delegation:\" << std::endl;\n",
    "    Vector2 v2(3., 7., 5.);\n",
    "    v2.Print();\n",
    "    \n",
    "    std::cout << \"Constructor that calls a delegate constructor:\" << std::endl;    \n",
    "    Vector2 v3(3.);\n",
    "    v3.Print();    \n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Default constructor\n",
    "\n",
    "If in a `class` or `struct` no constructor is defined, a default one is assumed: it takes no argument and sports an empty body.\n",
    "\n",
    "As soon as another constructor is defined, this default constructor no longer exists:\n",
    "   "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "struct ClassWithoutConstructor\n",
    "{ };"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    ClassWithoutConstructor my_object;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "struct ClassWithConstructorWithArg\n",
    "{\n",
    "    ClassWithConstructorWithArg(int a);\n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ClassWithConstructorWithArg::ClassWithConstructorWithArg(int a)\n",
    "{ }"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    ClassWithConstructorWithArg my_object; // COMPILATION ERROR!\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This must seem messy at first sight, but doing otherwise would be nightmarish: if you define a very complex class that must be carefully initialized with well thought-out arguments, you do not want your end-user to bypass this with an inconsiderate call to a constructor without arguments!\n",
    "\n",
    "If you want to enable back constructor without arguments, you may do so by defining it explicitly. C++11 introduced a nice way to do so (provided you wish an empty body - if not define it explicitly yourself):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "struct ClassWithConstructorWithAndWithoutArg\n",
    "{\n",
    "    ClassWithConstructorWithAndWithoutArg() = default;\n",
    "        \n",
    "    ClassWithConstructorWithAndWithoutArg(int a);    \n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ClassWithConstructorWithAndWithoutArg::ClassWithConstructorWithAndWithoutArg(int a)\n",
    "{ }"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    ClassWithConstructorWithAndWithoutArg my_object; // OK!\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Good practice: provide in the data attribute declaration a default value\n",
    "\n",
    "In a constructor, you are expected to initialize properly all the data attributes. You can't expect a default behaviour if you fail to do so:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "struct BadlyInitialized\n",
    "{\n",
    "    int a_;\n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "{\n",
    "    BadlyInitialized my_object;\n",
    "    std::cout << \"Undefined behaviour: no guarantee for the value of the data attribute!: \" << my_object.a_ << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You are therefore supposed to define explicitly all the data attributes in all of your constructors. It was easy to get trumped by this in C++98/03: if you added a new data attribute and forgot to initialize it in one of your constructor, you would have undefined behaviour that is one of the worst bug to track down! (as on your machine/architecture you may have a \"good\" behaviour haphazardly).\n",
    "\n",
    "Fortunately, C++ 11 introduced a mechanism I strongly recommend to provide a default value:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "struct SafeClass\n",
    "{\n",
    "    int a_ { 5 }; // The default value is provided here in the class declaration.        \n",
    "    \n",
    "    SafeClass() = default;\n",
    "    \n",
    "    SafeClass(int new_value);\n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "SafeClass::SafeClass(int new_value)\n",
    ": a_(new_value)\n",
    "{ }"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "{\n",
    "    SafeClass no_Arg;\n",
    "    std::cout << \"If constructor doesn't change the value, default is used: \" << no_Arg.a_ << std::endl;\n",
    "    \n",
    "    \n",
    "    SafeClass modified(10);\n",
    "    std::cout << \"If constructor changes the value, choice is properly used: \" << modified.a_ << std::endl;\n",
    "   \n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Please notice doing so doesn't prevent you to use the efficient initialization of `a_` in the constructor with arguments: the values thus provided in the data attributes definitions are used only if the constructor doesn't supersede them.\n",
    "\n",
    "In the same spirit, if you get pointers as data attributes it is a good idea to set them by default to `nullptr`: this way you may check with an [`assert`](../5-UsefulConceptsAndSTL/1-ErrorHandling.ipynb#Assert) it has been correctly initialized before use."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Good practice: use `explicit` constructors by default\n",
    "\n",
    "Let's study the following case:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "struct ClassWithIntConstructor\n",
    "{\n",
    "    ClassWithIntConstructor(int a);\n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "ClassWithIntConstructor::ClassWithIntConstructor(int a)\n",
    "{\n",
    "    std::cout << \"Constructor called with argument \" << a << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    ClassWithIntConstructor my_object(5);\n",
    "    \n",
    "    my_object = 7; // Dubious but correct: assigning an integer!\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So what happens here? In fact, the compiler implicitly convert the integer read into a `ClassWithIntConstructor` constructor with an integer argument...\n",
    "\n",
    "There are situations in which this might be deemed the right thing to do (none to my mind but I guess it depends on your programming style) but more often than not it's not what is intended and a good old compiler yell would be much preferable.\n",
    "\n",
    "To do so, in C++ 11 you must stick the keyword **explicit** in the declaration in front of the constructor. Personally I tend to always provide it to my constructors, following the likewise advice by [Effective Modern C++](../bibliography.ipynb#Effective-Modern-C++).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "struct ClassWithExplicitIntConstructor\n",
    "{\n",
    "    explicit ClassWithExplicitIntConstructor(int a);\n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "ClassWithExplicitIntConstructor::ClassWithExplicitIntConstructor(int a)\n",
    "{\n",
    "    std::cout << \"Constructor called with argument \" << a << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    ClassWithExplicitIntConstructor my_object(5);\n",
    "    \n",
    "    my_object = 7; // COMPILATION ERROR! YAY!\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc-hr-collapsed": true,
    "toc-nb-collapsed": true
   },
   "source": [
    "## Destructor\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The pendant of the constructor is the **destructor**, which is called when the object is terminated. Contrary to constructors, there is only one destructor for a given class, and by design it takes no parameter.\n",
    "\n",
    "The syntax is like a constructor with no parameter with an additional `~` in front of the name."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "struct Array\n",
    "{\n",
    "    Array(int unique_id, std::size_t array_size);\n",
    "    \n",
    "    ~Array(); // Destructor!    \n",
    "    \n",
    "    double* underlying_array_ = nullptr;\n",
    "    const int unique_id_;\n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Array::Array(int unique_id, std::size_t array_size)\n",
    ": unique_id_(unique_id)\n",
    "{\n",
    "    underlying_array_ = new double[array_size];\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "Array::~Array()\n",
    "{\n",
    "    std::cout << \"Memory for array \" << unique_id_ << \" is properly freed here.\" << std::endl;\n",
    "    delete[] underlying_array_;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    Array array1(1, 5ul);\n",
    "    \n",
    "    {\n",
    "        Array array2(2, 3ul);\n",
    "        \n",
    "        {\n",
    "            Array array3(3, 5ul);\n",
    "        }\n",
    "        \n",
    "        Array array4(4, 2ul);        \n",
    "    }\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It's important to notice the ordering here: as soon as an object becomes out of scope, it is immediately destroyed; the creation order doesn't matter at all!\n",
    "\n",
    "We will see a bit [later](/notebooks/5-UsefulConceptsAndSTL/2-RAII.ipynb) how to take advantage of this behaviour to write programs that do not leak memory.\n",
    "\n",
    "\n",
    "### Default destructor\n",
    "\n",
    "If not specified, C++ implicitly defines a destructor with an empty body. Personally I like even in this case to make it explicit, which is done the same way as for a constructor from C++11 onward:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "struct MyClass\n",
    "{\n",
    "    \n",
    "    MyClass() = default; // explicit default constructor\n",
    "    \n",
    "    ~MyClass() = default; // explicit default destructor\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is however a matter of personal taste; see for instance [this post from FluentCpp](https://www.fluentcpp.com/2019/04/23/the-rule-of-zero-zero-constructor-zero-calorie) for the opposite advice of not defining explicitly default constructor / destructor if you don't have to."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[© Copyright](../COPYRIGHT.md)   \n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Cppyy",
   "language": "c++",
   "name": "cppyy"
  },
  "language_info": {
   "codemirror_mode": "c++",
   "file_extension": ".cpp",
   "mimetype": "text/x-c++src",
   "name": "c++"
  },
  "latex_envs": {
   "LaTeX_envs_menu_present": true,
   "autoclose": false,
   "autocomplete": true,
   "bibliofile": "biblio.bib",
   "cite_by": "key",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 1,
   "hotkeys": {
    "equation": "Ctrl-E",
    "itemize": "Ctrl-I"
   },
   "labels_anchors": false,
   "latex_user_defs": false,
   "report_style_numbering": false,
   "user_envs_cfg": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
