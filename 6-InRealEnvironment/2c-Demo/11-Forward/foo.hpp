// File foo.hpp

#ifndef FOO_HPP
# define FOO_HPP

// Forward declaration: we say a class Bar is meant to exist...
class Bar;

struct Foo
{
    Foo(int n);
    
    void Print() const;

    Bar* bar_ = nullptr;
};

#endif // FOO_HPP