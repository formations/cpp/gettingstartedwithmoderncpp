# How to use Docker for this example?

The image is based on Fedora and is created through this [Dockerfile](../docker/Dockerfile.fedora_with_boost).

To run it, place yourself in the directory `6-InRealEnvironment/4b-Demo ` (where current `README.md` may be found) and type:

```shell
docker run -it --rm -v $PWD:/home/dev_cpp/training_cpp --cap-drop=all registry.gitlab.inria.fr/formations/cpp/gettingstartedwithmoderncpp/fedora_with_boost:latest
```

Most options have already been covered in the [main README](../README.md); the only new one is:

- `-it` which specifies the container must be run in interactive mode (i.e. you get a prompt to a terminal inside the Docker environment).

# Step by step: illustrating the issue

**NOTE** All the commands listed below must be run _INSIDE_ the container.

## Illustrating the issue

Go in the first directory:

```shell
cd 1-ProblematicCase/
```

You will see there the `simple_boost.cpp` presented in the notebook, and a CMakeLists.txt to compile it with CMake.

Please proceed:

```shell
mkdir build_clang
cd build_clang
cmake -G Ninja -DCMAKE_C_COMPILER=clang -DCMAKE_CXX_COMPILER=clang++ -DCMAKE_CXX_EXTENSIONS=OFF ..
ninja
```

At the time of this writing, I get 1640 warnings! (more than in the first run one year ago with Boost 1.69 or the training we gave in 2022, for which we got _only_ 1335 warnings...)

The reason for the increase of these warnings over time is not that Boost developers got sloppy, but that compilers vendors add new ones regularly (we will see that shortly when we'll control them through pragmas).

## First solution: system directories

Go in the second directory:

```shell
cd ../../2-FixByBuildSystem
```

The only change is in the CMakeLists.txt file: 

```cmake
target_include_directories(simple_boost PUBLIC "/opt/include")
```

is replaced by:

```cmake
target_include_directories(simple_boost SYSTEM PUBLIC "/opt/include")
```

And now the compilation works without warnings (from third party at least!):

```shell
mkdir build_clang
cd build_clang
cmake -G Ninja -DCMAKE_C_COMPILER=clang -DCMAKE_CXX_COMPILER=clang++ -DCMAKE_CXX_EXTENSIONS=OFF ..
ninja
```

## Second solution: pragmas

### Fixing clang warnings

Go in the third directory:

```shell
cd ../../3-FixByPragmas-clang
```

This time, `CMakeLists.txt` is exactly the same as in `1-ProblematicCase` but the `simple_boost.cpp` is amended: includes are surrounded by clang pragmas to deactivate warnings.

```shell
mkdir build_clang
cd build_clang
cmake -G Ninja -DCMAKE_C_COMPILER=clang -DCMAKE_CXX_COMPILER=clang++ -DCMAKE_CXX_EXTENSIONS=OFF ..
ninja
```

and we're all right! At least if we stick with clang... Let's compile with gcc to see:

```shell
cd ..
mkdir build_gcc
cd build_gcc
cmake -G Ninja -DCMAKE_C_COMPILER=gcc -DCMAKE_CXX_COMPILER=g++ -DCMAKE_CXX_EXTENSIONS=OFF ..
ninja
```

and we get two types of warnings:

* `-Wredundant-decls` 
* `-Wunknown-pragmas` as pragmas used for clang build are not recognized! So fixing for clang makes it somewhat worse for gcc.. (not completely - some ignored warnings do also exist in gcc and were therefore silenced).

We can see here that `clang` is much less forgiving than `gcc` regarding warnings; it issues more than one thousand of them, against only one about a redundant declaration in gcc!

That's the reason clang is my choice compiler when developing new features (along with the faster compilation time and the usually more informative compiler error messages...)

### Fixing clang AND gcc warnings

Go in the fourth directory:

```shell
cd ../../4-FixByPragmas-clang-gcc/
```

As for the case we just saw, only the source file is modified. We add here macros to separate clearly gcc and clang cases:

```shell
mkdir build_clang
cd build_clang
cmake -G Ninja -DCMAKE_C_COMPILER=clang -DCMAKE_CXX_COMPILER=clang++ -DCMAKE_CXX_EXTENSIONS=OFF ..
ninja
cd ..
mkdir build_gcc
cd build_gcc
cmake -G Ninja -DCMAKE_C_COMPILER=gcc -DCMAKE_CXX_COMPILER=g++ -DCMAKE_CXX_EXTENSIONS=OFF ..
ninja
```

And both now works without warnings from third party libraries.

### Fixing clang AND gcc warnings a bit more gracefully

Go in the fifth directory:

```shell
cd ../../5-FixByPragmas-common/
```

There are two changes here:

* A specific header file has been devised to include properly Boost filesystem. This way, if later you need it somewhere else you may include this file directly and get the right output in just one include line.
* More a matter of taste: I used macro magic to put in common the pragma commands that may work for both gcc and clang.

```shell
mkdir build_clang
cd build_clang
cmake -G Ninja -DCMAKE_C_COMPILER=clang -DCMAKE_CXX_COMPILER=clang++ -DCMAKE_CXX_EXTENSIONS=OFF ..
ninja
cd ..
mkdir build_gcc
cd build_gcc
cmake -G Ninja -DCMAKE_C_COMPILER=gcc -DCMAKE_CXX_COMPILER=g++ -DCMAKE_CXX_EXTENSIONS=OFF ..
ninja
```



