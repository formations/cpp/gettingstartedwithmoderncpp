Table of Contents
=================

- [Table of Contents](#table-of-contents)
  - [About this tutorial](#about-this-tutorial)
  - [Goal of this tutorial](#goal-of-this-tutorial)
  - [How to run it?](#how-to-run-it)
    - [BinderHub](#binderhub)
    - [Local installation](#local-installation)
      - [On Unix systems (Linux, macOS)](#on-unix-systems-linux-macos)
      - [On Windows](#on-windows)
    - [Docker](#docker)
  - [For maintainers and contributors](#for-maintainers-and-contributors)


## About this tutorial

This tutorial is heavily inspired from a C++ tutorial created by David Chamont (CNRS) that was given as a lecture with the help of Vincent Rouvreau (Inria) in 2016; latest version of this tutorial used as the basis of current one may be found [there](https://gitlab.inria.fr/FormationCpp/DebuterEnCpp).

Current version provides two major modifications:

* The tutorial is now in english.
* Jupyter notebooks using [homemade Cppyy kernel](https://gitlab.inria.fr/sed-saclay/cppyy_kernel) are now used, thus enabling a sort of interpreted C++ which is rather helpful for teaching it.


We have rewritten entirely and modified heavily several chapters, but the backbone remains heavily indebted to David and Vincent and the hands-on is still very similar to the original one.

As the original tutorial, the present lecture is released under the [Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)](http://creativecommons.org/licenses/by-nc-sa/4.0/) licence.


## Goal of this tutorial

This is an introductory lecture to the modern way of programming C++; at the end of it you should:

* Understand the syntax and basic mechanisms of the C++ language in version 14/17.
* Know the different programming styles.
* Know of and be able to use the most useful part of the standard library.
* Be aware of many good programming practices in C++.


## How to run it?

You may run the notebooks by one of the three following methods:

- From your browser through a Binder instance.
- Directly on your computer system.
- Through a Docker image on your computer.

Quick guides for each of these methods are given below.

### BinderHub

A link to a BinderHub instance is given at the top of the project page; foresee few minutes to set up properly the notebooks.

The pro of using Binder is that you have basically nothing more to do than click on the link; the cons are the delay to create the instance, the need for an Internet connection and the fact you won't keep locally the possible modifications you did.


### Local installation

#### On Unix systems (Linux, macOS)

* The dependencies to run this tutorial are:

- Jupyterlab
- Our dedicated [C++ Jupyter kernel](https://gitlab.inria.fr/sed-saclay/cppyy_kernel).


```shell
pip install jupyterlab
pip install git+https://gitlab.inria.fr/sed-saclay/cppyy_kernel.git
```

You may of course choose another way to install JupyterLab (typically using Anaconda or Mamba)


* Then you can run the notebook by going **into its root directory** (or internal links won't work...) in a terminal and typing:

```shell
jupyter lab
```

__NOTE__: It is possible to use the notebooks directly from some IDEs like [VSCode](https://gitlab.inria.fr/formations/cpp/gettingstartedwithmoderncpp/-/wikis/home#using-notebooks-with-vscode).

#### On Windows

None of us is using Windows, and Cppyy kernel was not tested for this OS at all.

Your best option if you're using Windows 10 or 11 is probably to  [install Ubuntu in your Windows session](https://tutorials.ubuntu.com/tutorial/tutorial-ubuntu-on-windows).



### Docker

It is possible to execute the notebooks in a Docker container.

First get the image from Gitlab registry:

```shell
docker login registry.gitlab.inria.fr
docker pull registry.gitlab.inria.fr/sed-saclay/cppyy_kernel/cppyy_kernel
```

Then run a container with:

```shell
docker run --rm -e JUPYTER_TOKEN='easy' -p 8888:8888  -v $PWD:/training_cpp -w /training_cpp --cap-drop=all registry.gitlab.inria.fr/sed-saclay/cppyy_kernel/cppyy_kernel:latest 
```

And in your browser type `http://localhost:8888`

and then type `easy` in the token dialog box (of course you may replace by whatever you want).


Few hints for those not familiar with Docker:

* `-v` creates a mapping between local folder and the `/training_cpp` folder in the container; this enables you to edit the file from your comfy local environment and see the file edited this way in the Docker container.
* `--cap-drop=all` is a safety when you're running a Docker image not built by yourself: you're essentially blocking the few remaining operations that might impact your own environment that Docker lets by default open with the run command.
* `-p` gives the port mapping between the Docker image and your local environment.
* `--rm` tells docker to delete the container after its use.
* `-w` tells to use as working directory the provided argument - which is here the mapped local folder.

The lengthy `registry.gitlab.inria.fr/formations/cpp/gettingstartedwithmoderncpp/xeus-cling-and-compilers` is the name of the Docker **image**, if this image is not present locally in your environment Docker will try to fetch it from a *registry* on the Inria Gitlab. 

Then just type [http://localhost:8888/](http://localhost:8888/) in your browser to run the notebooks.


## For maintainers and contributors

Information related to CI are [here](CI.md).
All contributions are welcome, but please read the [contribution guide](CONTRIBUTING.md) first.
