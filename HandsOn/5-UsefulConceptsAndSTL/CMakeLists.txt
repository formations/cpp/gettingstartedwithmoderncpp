cmake_minimum_required(VERSION 3.9)

include(../Config/cmake/Settings.cmake)

project(GettingStartedWithModernCpp_HandsOn_ConceptSTL)

include(../Config/cmake/AfterProjectSettings.cmake)   

add_executable(initial initial_file.cpp)

# add_executable(exercise36 exercise36.cpp)
# add_executable(exercise37 exercise37.cpp)
# add_executable(exercise38 exercise38.cpp)
# add_executable(exercise39 exercise39.cpp)
# add_executable(exercise40 exercise40.cpp)
# add_executable(exercise41 exercise41.cpp)
# add_executable(exercise42 exercise42.cpp)
# add_executable(exercise43 exercise43.cpp)