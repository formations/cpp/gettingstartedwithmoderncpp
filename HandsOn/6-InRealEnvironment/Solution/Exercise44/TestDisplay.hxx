#pragma once

#include <cmath>
#include <iostream>

#include "Tools.hpp"

template<typename IntT>
void TestDisplay::PrintLine(int Nbits, double exact, double approx,
                            RoundToInteger do_round_to_integer,
                            std::string optional_string1, std::string optional_string2) const
{
    IntT error = RoundAsInt<IntT>(GetResolution() * std::fabs(exact - approx) / exact);
    
    std::cout << "[With " << Nbits << " bits]: " << optional_string1;

    // Here we stop using the ternary operator used so far to handle more properly the warning about
    // long to double conversion. We use a switch but if/else would be almost as fine!
    // (the reason for the "almost" are explained in the dedicated notebook in appendix)
    switch(do_round_to_integer)
    {
        case RoundToInteger::yes:
            std::cout << RoundAsInt<IntT>(exact);
            break;
        case RoundToInteger::no:
            std::cout << exact;
            break;
    }

    std::cout << " ~ " << approx
        << optional_string2
        << "  [error = ";
        
    DisplayInteger(std::cout, error);
    
    std::cout << "/" << GetResolution() << "]" 
        << std::endl;    
}
