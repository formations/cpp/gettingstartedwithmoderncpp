#pragma once

#include "TestDisplay.hpp"

//! Class in charge of the display of the sum of 2 `PowerOfTwoApprox` with real coefficients.
template<typename IntT>
class TestDisplaySumOfMultiply : public TestDisplay
{
public:

    //! Constructor.
    TestDisplaySumOfMultiply(int resolution);

    //! Destructor. 
    virtual ~TestDisplaySumOfMultiply() override;
    
    //! Display the output for the chosen `Nbits`.
    void operator()(int Nbits) const override;
        
private:
    
    //! Method in charge of the actual display.
    void Display(int Nbits, double value1, IntT coefficient1, double value2, IntT coefficient2) const;
    
};

#include "TestDisplaySumOfMultiply.hxx"
