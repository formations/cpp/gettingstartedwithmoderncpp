#pragma once

#include "Tools.hpp"


template<typename IntT>
PowerOfTwoApprox<IntT>::PowerOfTwoApprox(int Nbits, double value)
{
    auto max_numerator = MaxInt<IntT>(Nbits);
    
    auto& numerator = numerator_; // alias!
    auto& exponent = exponent_; // alias!

    IntT denominator {};
    
    do
    {
        // I used here the prefix increment '++exponent' but you may put it on a separate line if you're not 
        // comfortable with it.
        HelperComputePowerOf2Approx(value, ++exponent, numerator, denominator);
    }
    while (numerator <= max_numerator);
    
    // After the while loop we have numerator > max_numerator,  
    // hence we need to update the fraction using the previous exponent with --exponent.
    HelperComputePowerOf2Approx(value, --exponent, numerator, denominator);
}


template<typename IntT>
PowerOfTwoApprox<IntT>::operator double() const
{
   IntT denominator = TimesPowerOf2(static_cast<IntT>(1), GetExponent());
   return static_cast<double>(GetNumerator()) / static_cast<double>(denominator);
}


template<typename IntT>
IntT PowerOfTwoApprox<IntT>::GetNumerator() const
{
    return numerator_;
}

template<typename IntT>
int PowerOfTwoApprox<IntT>::GetExponent() const
{
    return exponent_;
}

template<typename IntT>
IntT operator*(IntT coefficient, const PowerOfTwoApprox<IntT>& approx)
{
    IntT product;

    if (__builtin_mul_overflow(approx.GetNumerator(), coefficient, &product))
        throw Exception("in Overflow! (in operator*(IntT, const PowerOfTwoApprox<IntT>&))");

    return TimesPowerOf2(product, -approx.GetExponent());
}

template<typename IntT>
IntT operator*(const PowerOfTwoApprox<IntT>& approx, IntT coefficient)
{
    return coefficient * approx;
}

template<typename IntT>
std::ostream& operator<<(std::ostream& out, const PowerOfTwoApprox<IntT>& approximation)
{
    DisplayInteger<IntT>(out, approximation.GetNumerator());
    out << " / 2^" << approximation.GetExponent();
    return out;
}



template<typename IntT>
void HelperComputePowerOf2Approx(double value, int exponent, IntT& numerator, IntT& denominator)
{
    denominator = TimesPowerOf2(static_cast<IntT>(1), exponent);   
    numerator = RoundAsInt<IntT>(value * static_cast<double>(denominator));
}
