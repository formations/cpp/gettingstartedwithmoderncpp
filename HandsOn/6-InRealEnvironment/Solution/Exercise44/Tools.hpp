#pragma once

    
//! Returns `number` * (2 ^ `exponent`) 
template<typename IntT>
IntT TimesPowerOf2(IntT number, int exponent);

//! Round `x` to the nearest integer.
template<typename IntT>
IntT RoundAsInt(double x);


//! Maximum integer that might be represented with `nbits` bits.  
template<typename IntT>
IntT MaxInt(int nbits);


//! Simply display `integer` on `out` output stream in general case
//! Converts its first to an `int` if it is a `char`.
template<typename IntT>
void DisplayInteger(std::ostream& out, IntT integer);

#include "Tools.hxx"
