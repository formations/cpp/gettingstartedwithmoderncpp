#include "Exception.hpp"
#include "TestDisplay.hpp"
#include "Tools.hpp"


TestDisplay::TestDisplay(int resolution)
: resolution_(resolution)
{ }


TestDisplay::~TestDisplay() = default;




void TestDisplay::PrintOverflow(int Nbits, const Exception& e) const
{
    std::cout << "[With " << Nbits << " bits]: " << e.what() << std::endl;
}


int TestDisplay::GetResolution() const
{
    return resolution_;
}


void TestDisplay::Unused() const
{
    // We could have left this method empty... The error handling is just to tell user that might think this method
    // calls does anything that it doesn't.
    // In production code I would have used `assert(false && "This method is defined to address the Wweak-vtables 
    // warning; it is not intended to be called!");`; we'll see that later in ErrorHandling notebook.
    throw Exception("This method is defined to address the Wweak-vtables warning; it is not intended to be called!");
}
